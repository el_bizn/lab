﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LuxuryBags.BagLibr
{
    class BagInfo
    {
        private string _bagBrand = "";
        private string _bagModel = "";
        private string _bagColor = "";
        private double _bagPrice = 0;

        public string BagBrand
        {
            get
            {
                return _bagBrand;
            }
            set
            {
                if (value != null)
                    _bagBrand = value;
            }
        }
        public string BagModel
        {
            get
            {
                return _bagModel;
            }
            set
            {
                if (value != null)
                    _bagModel = value;
            }
        }
        public string BagColor
        {
            get
            {
                return _bagColor;
            }
            set
            {
                if (value != null)
                    _bagColor = value;
            }
        }
        public double BagPrice
        {
            get
            {
                return _bagPrice;
            }
            set
            {
                if (value != null)
                    _bagPrice = value;
            }
        }
    }
}
